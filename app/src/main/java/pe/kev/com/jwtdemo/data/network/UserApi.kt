package pe.kev.com.jwtdemo.data.network

import pe.kev.com.jwtdemo.data.responses.UsersResponse
import retrofit2.http.GET

interface UserApi {

    @GET("users")
    suspend fun getUsers():UsersResponse


    //mi api no tiene un endpoint para desloguear
    suspend fun logout()
}