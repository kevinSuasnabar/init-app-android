package pe.kev.com.jwtdemo.ui.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import pe.kev.com.jwtdemo.data.network.UserApi
import pe.kev.com.jwtdemo.data.repository.BaseRepository

abstract class BaseViewModel(
    private val repository: BaseRepository
) : ViewModel() {

    suspend fun logout(api: UserApi) = withContext(Dispatchers.IO) {
        repository.logout(api)
    }
}