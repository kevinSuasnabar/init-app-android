package pe.kev.com.jwtdemo.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import pe.kev.com.jwtdemo.data.network.Resource
import pe.kev.com.jwtdemo.data.network.UserApi
import pe.kev.com.jwtdemo.data.repository.UserRepository
import pe.kev.com.jwtdemo.data.responses.User
import pe.kev.com.jwtdemo.databinding.FragmentHomeBinding
import pe.kev.com.jwtdemo.ui.base.BaseFragment
import pe.kev.com.jwtdemo.ui.visible


class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding, UserRepository>() {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.progressbar.visible(false)


        viewModel.getUsers()

        viewModel.users.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Success -> {
                    binding.progressbar.visible(false)
                    updateUI(it.value.users)
                }
                is Resource.Loading -> {
                    binding.progressbar.visible(true)

                }
            }
        })

        binding.buttonLogout.setOnClickListener{
            logout()
        }
    }

    private fun updateUI(usersResponse: List<User>) {
        with(binding) {//genera un scope donde puedo usar las variables del binding sin usar el binding antes de cada atributo
            totalUsers.text = usersResponse.size.toString();
            firstUser.text = usersResponse.get(0).username.toString()
        }
    }

    override fun getViewModel() = HomeViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentHomeBinding.inflate(inflater, container, false)

    override fun getFragmentRepository(): UserRepository {
        //runBlocking para convertir de sincrono a asyncrono
        val token = runBlocking {
            userPreferences.authToken.first()
        }
        val api = remoteDataSource.buildApi(UserApi::class.java, token)
        return UserRepository(api)
    }


}