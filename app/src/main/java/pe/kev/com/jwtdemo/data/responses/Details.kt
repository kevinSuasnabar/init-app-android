package pe.kev.com.jwtdemo.data.responses

data class Details(
    val createdAt: String,
    val id: Int,
    val lastname: Any,
    val name: Any,
    val status: String,
    val updateAt: String
)