package pe.kev.com.jwtdemo.data.responses

data class ProductResponse(
    val items: List<Product>,
    val totalQuantity: Int,
    val totalPages:Int
)