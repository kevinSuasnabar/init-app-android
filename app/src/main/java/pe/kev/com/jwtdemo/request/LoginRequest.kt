package pe.kev.com.jwtdemo.request

data class LoginRequest(
    val password: String,
    val username: String
)