package pe.kev.com.jwtdemo.data.network

import pe.kev.com.jwtdemo.request.LoginRequest
import pe.kev.com.jwtdemo.data.responses.LoginResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {

    @POST("auth/signin")
    suspend fun login(
        @Body loginRequest: LoginRequest
//        @Field("username") username: String,
//        @Field("password") password: String
    ): LoginResponse //cambiar por mi response de login
}