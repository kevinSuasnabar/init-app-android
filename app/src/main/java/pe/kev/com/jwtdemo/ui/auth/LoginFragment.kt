package pe.kev.com.jwtdemo.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import pe.kev.com.jwtdemo.databinding.FragmentLoginBinding
import pe.kev.com.jwtdemo.data.network.AuthApi
import pe.kev.com.jwtdemo.data.network.Resource
import pe.kev.com.jwtdemo.data.repository.AuthRepository
import pe.kev.com.jwtdemo.ui.base.BaseFragment
import pe.kev.com.jwtdemo.ui.enable
import pe.kev.com.jwtdemo.ui.handleApiError
import pe.kev.com.jwtdemo.ui.home.HomeActivity
import pe.kev.com.jwtdemo.ui.startNewActivity
import pe.kev.com.jwtdemo.ui.visible


class LoginFragment : BaseFragment<AuthViewModel, FragmentLoginBinding, AuthRepository>() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.progressbar.visible(false)
        binding.buttonLogin.enable(false)


        /**Paso 2 observar respuesta*/
        viewModel.loginResponse.observe(viewLifecycleOwner, Observer { it ->
            binding.progressbar.visible(it is Resource.Loading)
            when (it) {
                is Resource.Success -> {
                    lifecycleScope.launch {
                        viewModel.saveAuthToken(it.value.token!!)//guardamos con datasotre
                        requireActivity().startNewActivity(HomeActivity::class.java)
                    }
                }
                is Resource.Failure -> handleApiError(it) { login() }
            }
        })

        //enabled or disabled button cuando se escribe el correo y password
        binding.editTextTextPassword.addTextChangedListener {
            val email = binding.editTextTextEmailAddress.text.toString().trim()
            binding.buttonLogin.enable(email.isNotEmpty() && it.toString().isNotEmpty())
        }


        /**Paso 1 enviar al servicio*/
        binding.buttonLogin.setOnClickListener {
            login()
        }

    }

    private fun login() {
        val email = binding.editTextTextEmailAddress.text.toString().trim() //or username
        val password = binding.editTextTextPassword.text.toString().trim()

        viewModel.login(email, password)
    }

    override fun getViewModel() = AuthViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentLoginBinding.inflate(inflater, container, false)

    override fun getFragmentRepository() =
        AuthRepository(remoteDataSource.buildApi(AuthApi::class.java), userPreferences)


}