package pe.kev.com.jwtdemo.data.responses

data class UsersResponse(
    val users: List<User>
)