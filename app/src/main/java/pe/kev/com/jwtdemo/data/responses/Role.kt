package pe.kev.com.jwtdemo.data.responses

data class Role(
    val createdAt: String,
    val description: String,
    val id: Int,
    val name: String,
    val status: String,
    val updateAt: String
)