package pe.kev.com.jwtdemo.ui.home.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pe.kev.com.jwtdemo.data.network.Resource
import pe.kev.com.jwtdemo.data.repository.ProductRepository
import pe.kev.com.jwtdemo.data.responses.ProductResponse
import pe.kev.com.jwtdemo.ui.base.BaseViewModel

class ProductViewModel(
    private val repository: ProductRepository
) : BaseViewModel(repository) {

    private val _products: MutableLiveData<Resource<ProductResponse>> = MutableLiveData()
    val products: LiveData<Resource<ProductResponse>>
        get() = _products

    fun getProducts(
        page: Int
    ) = viewModelScope.launch {
        _products.value = Resource.Loading//para mostrar el loading
        _products.value = repository.getProducts(page)

    }



}