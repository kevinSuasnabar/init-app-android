package pe.kev.com.jwtdemo.ui.home.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.merge
import pe.kev.com.jwtdemo.R
import pe.kev.com.jwtdemo.data.network.ProductApi
import pe.kev.com.jwtdemo.data.network.Resource
import pe.kev.com.jwtdemo.data.network.UserApi
import pe.kev.com.jwtdemo.data.repository.ProductRepository
import pe.kev.com.jwtdemo.data.repository.UserRepository
import pe.kev.com.jwtdemo.data.responses.Product
import pe.kev.com.jwtdemo.databinding.FragmentProductBinding
import pe.kev.com.jwtdemo.ui.base.BaseFragment
import pe.kev.com.jwtdemo.ui.home.products.adapter.ProductAdapter
import pe.kev.com.jwtdemo.ui.snackbar
import pe.kev.com.jwtdemo.ui.visible


class ProductFragment :
    BaseFragment<ProductViewModel, FragmentProductBinding, ProductRepository>(),
    ProductAdapter.OnProductClickListener {

    var currentPage: Int = 0
    var totalPages: Int = 0
    private lateinit var adapter: ProductAdapter;
    private var items: MutableList<Product> = mutableListOf()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doInitialization()
    }

    private fun doInitialization() {
        binding.rvProducts.setHasFixedSize(true)
        viewModel.getProducts(currentPage)
        adapter = ProductAdapter(items, this@ProductFragment)
        binding.rvProducts.adapter = adapter

        binding.rvProducts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!binding.rvProducts.canScrollVertically(1)) {
                    if (currentPage+1 <= totalPages){
                        currentPage++;
                        binding.progressBarMore.visible(true)
                        viewModel.getProducts(currentPage)
                    }else{
                        requireView().snackbar("No hay mas productos")
                    }
                }
            }
        })

        getProducts()
    }

    private fun getProducts() {
        viewModel.products.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Resource.Success -> {
                    binding.progressBar.visible(false)
                    binding.progressBarMore.visible(false)
                    var oldCount:Int = items.size
                    items.addAll(it.value.items)
                    totalPages = it.value.totalPages
                    adapter.notifyItemRangeInserted(oldCount,items.size)
                }
                is Resource.Loading -> {
                    binding.progressBar.visible(true)
                }
            }

        })
    }


    override fun getViewModel() = ProductViewModel::class.java

    override fun getFragmentBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentProductBinding.inflate(inflater, container, false)

    override fun getFragmentRepository(): ProductRepository {
        val api = remoteDataSource.buildApi(ProductApi::class.java)
        return ProductRepository(api)
    }


    override fun onProductClick(product: Product) {
        requireView().snackbar("${product.name}")
    }


}