package pe.kev.com.jwtdemo.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import pe.kev.com.jwtdemo.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}