package pe.kev.com.jwtdemo.data.network

import pe.kev.com.jwtdemo.data.responses.Product
import pe.kev.com.jwtdemo.data.responses.ProductResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ProductApi {

    @GET("product/findAll")
    suspend fun getProducts(
        @Query("page") page: Int
    ): ProductResponse
}