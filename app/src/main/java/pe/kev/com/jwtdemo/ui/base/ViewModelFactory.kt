package pe.kev.com.jwtdemo.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import pe.kev.com.jwtdemo.data.repository.AuthRepository
import pe.kev.com.jwtdemo.data.repository.BaseRepository
import pe.kev.com.jwtdemo.data.repository.ProductRepository
import pe.kev.com.jwtdemo.data.repository.UserRepository
import pe.kev.com.jwtdemo.ui.auth.AuthViewModel
import pe.kev.com.jwtdemo.ui.home.HomeViewModel
import pe.kev.com.jwtdemo.ui.home.products.ProductViewModel
import java.lang.IllegalArgumentException

class ViewModelFactory(
    private val repository: BaseRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(AuthViewModel::class.java) -> AuthViewModel(repository as AuthRepository) as T
            modelClass.isAssignableFrom(HomeViewModel::class.java)->HomeViewModel(repository as UserRepository) as T
            modelClass.isAssignableFrom(ProductViewModel::class.java)->ProductViewModel(repository as ProductRepository) as T
            //se pueden agregar mas
            else -> throw IllegalArgumentException("ViewModelClass Not Fount")

        }
    }
}