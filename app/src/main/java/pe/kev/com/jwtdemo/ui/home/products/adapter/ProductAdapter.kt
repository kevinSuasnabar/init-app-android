package pe.kev.com.jwtdemo.ui.home.products.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.merge
import pe.kev.com.jwtdemo.data.responses.Product
import pe.kev.com.jwtdemo.databinding.ProductItemBinding
import pe.kev.com.jwtdemo.ui.base.BaseViewHolder

class ProductAdapter(
    private val productList: List<Product>,
    private val itemClickListener: OnProductClickListener
) : RecyclerView.Adapter<BaseViewHolder<*>>() {


    /**Interface que tomara el evento click*/
    interface OnProductClickListener {
        fun onProductClick(product: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding =
            ProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ProductViewHolder(itemBinding, parent.context)
        itemBinding.buttonAdd.setOnClickListener {
            val position = holder.adapterPosition.takeIf { it != DiffUtil.DiffResult.NO_POSITION }
                ?: return@setOnClickListener // si no se obtiene una posicion solo me devuelve el evento sin ninguna posicion
            itemClickListener.onProductClick(productList[position])
        }
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is ProductViewHolder -> holder.bind(productList[position])
        }
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    /**Clase que se encarga de setear los datos a cada uno de los elementos de la lista*/
    private inner class ProductViewHolder(val binding: ProductItemBinding, val context: Context) :
        BaseViewHolder<Product>(binding.root) {

        override fun bind(item: Product) {
            binding.txtValueNombre.text = item.name;
            binding.txtValuePrecio.text = item.price;
        }

    }

}
