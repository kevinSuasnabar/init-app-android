package pe.kev.com.jwtdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import pe.kev.com.jwtdemo.data.UserPreferences
import pe.kev.com.jwtdemo.ui.auth.AuthActivity
import pe.kev.com.jwtdemo.ui.home.HomeActivity
import pe.kev.com.jwtdemo.ui.startNewActivity

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val userPreferences = UserPreferences(this)
        userPreferences.authToken.asLiveData().observe(this, Observer {
            //validamos si hay un token me manda a home si no me manda al login
            val activity = if (it == null) AuthActivity::class.java else HomeActivity::class.java
            startNewActivity(activity)
        })


    }


}