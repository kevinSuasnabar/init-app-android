package pe.kev.com.jwtdemo.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pe.kev.com.jwtdemo.data.network.Resource
import pe.kev.com.jwtdemo.data.repository.UserRepository
import pe.kev.com.jwtdemo.data.responses.UsersResponse
import pe.kev.com.jwtdemo.ui.base.BaseViewModel

class HomeViewModel(
    private val repository: UserRepository
) : BaseViewModel(repository) {

    private val _users: MutableLiveData<Resource<UsersResponse>> = MutableLiveData()
    val users: LiveData<Resource<UsersResponse>>
        get() = _users

    fun getUsers() = viewModelScope.launch {
        _users.value = Resource.Loading//para mostrar el loading
        _users.value = repository.getUsers()
    }


}