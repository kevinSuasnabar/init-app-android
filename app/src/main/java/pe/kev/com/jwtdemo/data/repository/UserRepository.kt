package pe.kev.com.jwtdemo.data.repository

import pe.kev.com.jwtdemo.data.network.UserApi

class UserRepository(
    private val api: UserApi
) : BaseRepository() {

    suspend fun getUsers() = safeApiCall {
        api.getUsers()
    }
}