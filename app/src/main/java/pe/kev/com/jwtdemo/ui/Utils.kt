package pe.kev.com.jwtdemo.ui

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.google.android.material.snackbar.Snackbar
import pe.kev.com.jwtdemo.data.network.Resource
import pe.kev.com.jwtdemo.ui.auth.LoginFragment
import pe.kev.com.jwtdemo.ui.base.BaseFragment

fun <A : Activity> Activity.startNewActivity(activity: Class<A>) {
    Intent(this, activity).also {
        it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(it)
    }
}

fun View.visible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.enable(enabled: Boolean) {
    isEnabled = enabled
    alpha = if (enabled) 1f else 0.5f
}

fun View.snackbar(message: String, action: (() -> Unit)? = null) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    action?.let {
        snackbar.setAction("Retry") {
            it()
        }
    }
    snackbar.show()
}

fun Fragment.handleApiError(
    failure: Resource.Failure,
    retry: (() -> Unit)? = null
) {
    when {
        failure.isNetworkError -> requireView().snackbar(
            "Please check you internet connection",
            retry
        )
        failure.errorCode == 401 -> {
            if (this is LoginFragment) {
                requireView().snackbar("Usuario no autorhizado")
            } else {
                //solo sirve si mi api tiene un endpoint que desloguea
//                (this as BaseFragment<*, *, *>).logout()

            }
        }
        failure.errorCode == 404 -> {
            if (this is LoginFragment) {
                requireView().snackbar("Datos incorrectos")
            }
        }
        else -> {
            val error = failure.errorBody.toString()
            requireView().snackbar(error)
        }

    }
}


fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}

