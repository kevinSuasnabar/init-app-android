package pe.kev.com.jwtdemo.data.repository

import pe.kev.com.jwtdemo.data.network.ProductApi

class ProductRepository(
    private val api: ProductApi
) : BaseRepository() {

    suspend fun getProducts(
        page: Int
    ) = safeApiCall {
        api.getProducts(page)
    }
}