package pe.kev.com.jwtdemo.data.responses

data class User(
    val createdAt: String,
    val details: Details,
    val email: String,
    val id: Int,
    val password: String,
    val roles: List<Role>,
    val status: String,
    val updateAt: String,
    val username: String
)