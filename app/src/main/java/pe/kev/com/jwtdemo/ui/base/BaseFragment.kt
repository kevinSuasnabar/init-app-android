package pe.kev.com.jwtdemo.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import pe.kev.com.jwtdemo.data.UserPreferences
import pe.kev.com.jwtdemo.data.network.RemoteDataSource
import pe.kev.com.jwtdemo.data.network.UserApi
import pe.kev.com.jwtdemo.data.repository.BaseRepository
import pe.kev.com.jwtdemo.ui.auth.AuthActivity
import pe.kev.com.jwtdemo.ui.startNewActivity

abstract class BaseFragment<VM : BaseViewModel, B : ViewBinding, R : BaseRepository> : Fragment() {

    //datastore
    protected lateinit var userPreferences: UserPreferences

    protected lateinit var binding: B
    protected lateinit var viewModel: VM
    protected val remoteDataSource = RemoteDataSource()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        /**datastore*/
        userPreferences = UserPreferences(requireContext())

        binding = getFragmentBinding(inflater, container)
        /**Viewmodels con parametros*/
        val factory = ViewModelFactory(getFragmentRepository())
        viewModel = ViewModelProvider(this, factory).get(getViewModel())

        lifecycleScope.launch {
            userPreferences.authToken.first()
        }
        return binding.root
    }

    fun logout() = lifecycleScope.launch {
        /**INICIO bloque trivial si mi api no tiene un endpoint de logout*/
        val authToken = userPreferences.authToken.first()
        val api = remoteDataSource.buildApi(UserApi::class.java, authToken)
        viewModel.logout(api)
        /**FIN bloque trivial si mi api no tiene un endpoint de logout*/

        userPreferences.clear()
        requireActivity().startNewActivity(AuthActivity::class.java)

    }

    abstract fun getViewModel(): Class<VM>
    abstract fun getFragmentBinding(inflater: LayoutInflater, container: ViewGroup?): B
    abstract fun getFragmentRepository(): R

}