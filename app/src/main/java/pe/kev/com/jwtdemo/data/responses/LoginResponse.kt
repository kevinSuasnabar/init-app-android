package pe.kev.com.jwtdemo.data.responses

data class LoginResponse(
    val email: String,
    val id: Int,
    val token: String,
    val username: String
)