package pe.kev.com.jwtdemo.data.repository

import pe.kev.com.jwtdemo.data.UserPreferences
import pe.kev.com.jwtdemo.data.network.AuthApi
import pe.kev.com.jwtdemo.request.LoginRequest

class AuthRepository(
    private val api: AuthApi,
    private val preferences: UserPreferences
) : BaseRepository() {

    /**FUNCION DE LOGIN*/
    suspend fun login(
        username: String,
        password: String
    ) = safeApiCall {
        api.login(LoginRequest(password, username))
    }

    suspend fun saveAuthToken(token: String) {
        preferences.saveAuthToken(token)
    }

}