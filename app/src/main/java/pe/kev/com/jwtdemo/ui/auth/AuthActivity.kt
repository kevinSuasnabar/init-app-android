package pe.kev.com.jwtdemo.ui.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import pe.kev.com.jwtdemo.R

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }
}