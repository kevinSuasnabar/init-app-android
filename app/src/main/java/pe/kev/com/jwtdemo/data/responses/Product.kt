package pe.kev.com.jwtdemo.data.responses

data class Product(
    val id: Int,
    val name: String,
    val price: String,
    val status: String,
)